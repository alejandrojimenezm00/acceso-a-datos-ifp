package uf2Ej1;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class Conector {

	Connection conn1 = null;

	public Conector(){
		
		try {
			
			// Con estas líneas declaro la URL para conectase a la BBDD y luego lanzando getConnection me conecto
			
			String url1 = "jdbc:mysql://localhost:3306/";
			String user = "root";
			String password = "";
			
			conn1 = DriverManager.getConnection(url1, user, password);
			
			if (conn1 != null) {
				System.out.println("Connectedto MYSQL");
			}

		} catch (SQLException ex) {
			System.out.println("ERROR:La dirección no es válida o el usuario y clave");
			ex.printStackTrace();
		}
	}

	
	// Con este metodo cierro la conexion
	
	public void closeConnexion(){
		
		try {

			System.out.println("Conexión terminada");
			conn1.close();

		} catch (SQLException ex) {
			System.out.println("ERROR:al cerrar la conexión");
			ex.printStackTrace();
		}
	}
}
	

