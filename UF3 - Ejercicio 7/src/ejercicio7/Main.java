package ejercicio7;

import java.io.File;
import java.io.IOException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Result;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.TransformerFactoryConfigurationError;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.DOMImplementation;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.w3c.dom.Text;
import org.xml.sax.SAXException;

public class Main {
	
	
	public static void main(String[] args) throws TransformerException {
	
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		
		try {
		
			// asignación de valor a las variables
			// Con estas variables se configura el programa para que acceda al XML y pueda escribir
			
			DocumentBuilder builder = factory.newDocumentBuilder();
			Document document = builder.parse(new File ("Personas.xml"));
			document.getDocumentElement().normalize();
			
			// Con esta linea leo que elemento es el principal en el documento
			System.out.printf("Elemento raiz: %s %n", document.getDocumentElement().getNodeName());
			
			// Con esta linea registro que elemento es el que quiero leer
			NodeList personas = document.getElementsByTagName("Persona");
			System.out.printf("Nodos personas a recorrer: %d %n", personas.getLength());
			
			// Con este for itero sobre los elementos que quiero leer y los imprimo por pantalla 
			for (int i = 0; i < personas.getLength(); i++) {
				
				Node emple = personas.item(i);
				if (emple.getNodeType() == Node.ELEMENT_NODE) {
					Element elemento = (Element) emple;
					
					System.out.printf("Nombre = %s %n", elemento.getElementsByTagName("nombre").item(0).getTextContent());
					System.out.printf("Apellido = %s %n", elemento.getElementsByTagName("apellido").item(0).getTextContent());
					System.out.printf("edad = %s %n", elemento.getElementsByTagName("edad").item(0).getTextContent());
					System.out.printf("trabaja = %s %n", elemento.getElementsByTagName("trabaja").item(0).getTextContent());

				}
				
			}
			
			
			
		} catch (ParserConfigurationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SAXException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	
	}

}
