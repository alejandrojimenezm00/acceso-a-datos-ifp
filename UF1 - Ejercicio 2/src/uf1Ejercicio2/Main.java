package uf1Ejercicio2;

import java.io.*;
import java.util.Scanner;
import uf1Ejercicio2.Persona;

public class Main {

	public static void main(String[] args) {
		
		// Objeto scanner para leer datos
        Scanner sc = new Scanner(System.in);

		// Creando un File path conseguimos comprobar la existencia del path
		
		File path = new File ("C:/ejercicio/");
		
		// Declaración del fichero a null. Dado a que si el directorio no existe no tiene sentido
		// reservar memoria al fichero.
		
		File nombresAlumnos = null;
		
		// Primero compruebo si el directorio existe. Sino lo hace no tiene sentido continuar con la
		// ejecución del código.
		
		if (!path.isDirectory()){
			
			System.out.println("El directorio no existe, Pongase en contacto con el administrador");
		}
		else {
			
			nombresAlumnos = new File (path + "/personas.txt");
			
			// Aqui creo el bucle para que el usuario cree las personas
			// Dado a que si el path es incorrecto no tiene sentido que el usuario reserve memoria o "pierda el tiempo"
			
			// Ya que el usuario va a crear 3 personas. Pienso que la mejor forma de almacenarlas es con un array
			
			Persona[] personas = new Persona[3];
			
			
			for (int i = 0; i < 3; i++) {
				
				// Llamada al constructor vacio.
				personas[i] = new Persona();
				
				System.out.println("Nombre: ");
				personas[i].setNombre(sc.nextLine());
				
				System.out.println("Apellido: ");
				personas[i].setApellido(sc.nextLine());
				
				// Aqui, antes de pasar al siguiente campo leo una linea vacia para limpiar el buffer
				System.out.println("Edad: ");
				personas[i].setEdad(sc.nextInt());
				sc.nextLine();
				
				System.out.println("Ciudad: ");
				personas[i].setCiudad(sc.nextLine());
				
				System.out.println("Nacionalidad: ");
				personas[i].setNacionalidad(sc.nextLine());
			}
			
			
			
			
			if (nombresAlumnos.exists())
			{
				System.out.println("El fichero existe, se van a introducir datos");
				
				try {
					
					// Linea para abrir el fichero en modo escritura. con el parametro true hago un append.
					// lo que me mantiene los datos que hubiese antes
					
					
					BufferedWriter bw = new BufferedWriter(new FileWriter(nombresAlumnos, true));
					
					for (int i = 0; i < 3; i++)
					{
						bw.write(personas[i].obtenerPersona());
						bw.write("\n");
					}
					
					// Con el metodo close() cierro el fichero y se guardan los cambios
					
					bw.close();
					
				} catch (IOException e) {
					
					System.out.println("Error al leer el fichero");

				}
				
			}
			else {
				
				System.out.println("El fichero no existe, se crea exitosamente");
				
				try {
					
					// Reutilizo la linea porque en caso de que no lo encuentre, BufferedWritter crea el fichero
					
					BufferedWriter bw = new BufferedWriter(new FileWriter(nombresAlumnos));
					
					for (int i = 0; i < 3; i++)
					{
						bw.write(personas[i].obtenerPersona());
						bw.write("\n");
					}
					
					// Con el metodo close() cierro el fichero y se guardan los cambios
					
					bw.close();
					
				} catch (IOException e) {
					
					System.out.println("Error al crear el fichero");
					
				}
			}

		}
			
		}
		
	}
