package ejercicio6;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Result;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.TransformerFactoryConfigurationError;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.DOMImplementation;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Text;

public class AccesoDatosUf3 {

	
	// Con este metodo añado los datos de la persona al XML
	
	static void CrearElemento(String datosEntrada, String valor, Element raiz, Document document) {
		
		Element elem = document.createElement(datosEntrada);
		Text text = document.createTextNode(valor);
		raiz.appendChild(elem);
		elem.appendChild(text);
		
	}
	
	
	public static void main(String[] args) throws TransformerException {
		
		// Declaración de variables para el funcionamiento del programa
		
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		DocumentBuilder constructor;
		DOMImplementation implantador;
		Document documento;
		Element raiz;
		
		try {
			
			// asignación de valor a las variables
			// Con estas variables se configura el programa para que acceda al XML y pueda escribir
			constructor = factory.newDocumentBuilder();
			implantador = constructor.getDOMImplementation();
			documento = implantador.createDocument(null, "Personas", null);
			documento.setXmlVersion("1.0");
			
			// Con las siguientes lineas añado el elemento persona al documento y le asigno los valores siguientes
			raiz = documento.createElement("Persona");
			documento.getDocumentElement().appendChild(raiz);
			
			CrearElemento("nombre", "Alejandro", raiz, documento);
			CrearElemento("apellido", "Jimenez", raiz, documento);
			CrearElemento("edad", "21", raiz, documento);
			CrearElemento("trabaja", "Si", raiz, documento);
			
			raiz = documento.createElement("Persona");
			documento.getDocumentElement().appendChild(raiz);
			
			CrearElemento("nombre", "Juan", raiz, documento);
			CrearElemento("apellido", "Perez", raiz, documento);
			CrearElemento("edad", "20", raiz, documento);
			CrearElemento("trabaja", "no", raiz, documento);
			
			raiz = documento.createElement("Persona");
			documento.getDocumentElement().appendChild(raiz);
			
			CrearElemento("nombre", "Antonio", raiz, documento);
			CrearElemento("apellido", "Perez", raiz, documento);
			CrearElemento("edad", "22", raiz, documento);
			CrearElemento("trabaja", "si", raiz, documento);
			
			// Con estas lineas creo el documento con los datos anteriores
			Source source = new DOMSource(documento);
			Result result = new StreamResult(new java.io.File("Personas.xml"));
			Transformer transformer;
			
			try {
				transformer = TransformerFactory.newInstance().newTransformer();
				transformer.transform(source, result);

			} catch (TransformerConfigurationException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (TransformerFactoryConfigurationError e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			System.out.println("Fin del programa");
			

		} catch (ParserConfigurationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

}
