package ejercicio9;

import java.io.FileNotFoundException;
import java.io.IOException;

import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.XMLReaderFactory;

import ejercicio9.GestionContenido;

public class Main {

	public static void main(String[] args) throws FileNotFoundException, IOException, SAXException{
		
		 // Con esta linea declaro el elemento que se va a encargar de poder leer el XML
		 XMLReader procesadorXML = XMLReaderFactory.createXMLReader();
		 
		 // Con esta linea instacio el objeto gestor
		 GestionContenido gestor= new GestionContenido();
		 
		 // Con esta linea le indicado al procesaroXML que abra el objeto
		 procesadorXML.setContentHandler(gestor);
		 
		 // Con esta linea indico que fichero tiene que abrir
		 InputSource fileXML = new InputSource("Personas.xml");
		 
		 // Con esta linea leo el fichero y añado lo datos al buffer para que se impriman
		 procesadorXML.parse(fileXML);

		 }

}
