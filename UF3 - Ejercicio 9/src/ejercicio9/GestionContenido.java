package ejercicio9;

import org.xml.sax.Attributes;
import org.xml.sax.helpers.DefaultHandler;

public class GestionContenido extends DefaultHandler{

	public GestionContenido() {
		super();
	}
	
	public void startDocument() {
		System.out.println("Comienzo el documento XML");
	}
	
	public void enDocument() {
		System.out.println("Final del documento");
	}
	
	public void startElement (String uri, String nombre, String nombreC, Attributes atts) {
		System.out.printf("\t Principio Elemento: %s %n", nombre);
	}
	
	public void endElement (String uri, String nombre, String nombreC, Attributes atts) {
		System.out.printf("\t Fin Elemento: %s %n", nombre);
	}
	
	public void characters (char[] ch, int inicio, int longitud) {
		
		String car = new String (ch, inicio, longitud);
		car = car.replaceAll("[\t\n]", "");
		System.out.printf("\t Caracteres: %s %n", car);
		
	}
	
	
}