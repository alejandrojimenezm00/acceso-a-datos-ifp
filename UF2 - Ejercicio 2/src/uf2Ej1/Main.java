package uf2Ej1;
import java.sql.Statement;
import java.util.Scanner;

public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		int eleccion = 0;
		Scanner sc = new Scanner(System.in);
		
		Conector conexion = new Conector();
		
		do {
			
			// Menu con las opciones del programa
			
			System.out.println("1: Crear Tabla\n"
			         + "2: Añadir una Montaña\n"
			         + "3: Consultar toda la tabla\n"
			         + "4: Consutlar los datos de una Montaña\n"
			         + "5: Editar campos\n"
			         + "6: Borrar una Montaña\n"
			         + "7: Borrar la tabla\n"
			         + "8: Salir\n"
			         + "Elección: ");
			
			eleccion = sc.nextInt();
			sc.nextLine();
			
			switch (eleccion) {
			
			case 1: {
				
				// LLamada al metodo para crear una tabla
				
				conexion.crearTabla();
				
			}
			case 2: {
				
				// Metodo que recoje datos del usuario para añadir una montaña a la tabla
				
				String nombre = "";
				int altura = 0;
				int primeraEscalada = 0;
				String region = "";
				String pais = "";
				
				System.out.println("Nombre de la montaña: ");
				nombre = sc.nextLine();
				System.out.println("Altura: ");
				altura = sc.nextInt();
				sc.nextLine();
				System.out.println("Primera escalada: ");
				primeraEscalada = sc.nextInt();
				sc.nextLine();
				System.out.println("Region de la montaña: ");
				region = sc.nextLine();
				System.out.println("Pais de la montaña: ");
				pais = sc.nextLine();
				
				conexion.crearMontania(nombre, altura, primeraEscalada, region, pais);
				
			}
			case 3: {
				
				// LLamada a un metodo que devuelve la tabla
				
				conexion.consultarTabla();
				
			}
			case 4: {
				
				// Opcion para consultar los datos de una montaña.
				// Lo primero que hago es consultar todas las montañas en la tabla con el metodo devolverMonania()
				// Se las muestro al usuario y con la respuesta que de llamo al metodo consultarMontania()
				
				String eleccionMontania = "";
				
				System.out.println("Estas son las montañas de las que se dispone en la BBDD:");
				conexion.devolverMonania();
				
				System.out.println("Consulta: ");
				eleccionMontania = sc.nextLine();
				
				conexion.consultarMontania(eleccionMontania);
				
			}
			case 5: {
				
				// Para la modificacion creo un submenu para que el usuario pueda modificar mas de un campo si así lo desea
				
				int eleccionMoficacion = 0;
				String  montaniaModificar = "";
				do {
					
					System.out.println("1: Modificar Nombre\n"
					         + "2: Modificar Altura\n"
					         + "3: Modificar primera ascension\n"
					         + "4: Modificar Region\n"
					         + "5: Modificar Pais\n"
					         + "6: Salir\n");
					
					eleccionMoficacion = sc.nextInt();
					sc.nextLine();
					
					if (eleccionMoficacion <= 5) {
						
										
						System.out.println("Montaña que quieres modificar: ");
						montaniaModificar = sc.nextLine();
						
						System.out.println("Los datos de la montaña que quieres modifcicar son: ");
						conexion.consultarMontania(montaniaModificar);
						
						switch (eleccionMoficacion) {
						
						case (1) : conexion.modificarMontania(montaniaModificar, "Nombre");
						case (2) : conexion.modificarMontaniaNum(montaniaModificar, "Altura");
						case (3) : conexion.modificarMontaniaNumAscension(montaniaModificar, "PrimeraAscension");
						case (4) : conexion.modificarMontania(montaniaModificar, "Región");
						case (5) : conexion.modificarMontania(montaniaModificar, "País");
						
						}
						
						eleccionMoficacion = 0;
						
					}

					
				} while (eleccionMoficacion <= 5);
				
			}
			case (6) : {
				
				// Metodo para eliminar montañas
				
				String eleccionMontania = "";
				
				System.out.println("Estas son las montañas de las que se dispone en la BBDD:");
				conexion.devolverMonania();
				
				System.out.println("Montaña a eliminar: ");
				eleccionMontania = sc.nextLine();
				
				conexion.eliminarMonania(eleccionMontania);
				
			}
			case (7) : {
				
				// Llamada a metodo para eliminar la tablaD
				
				conexion.eliminarTabla();
				
			}
			
			}
			
		} while (eleccion < 8);
		
		conexion.closeConnexion();
	}

}
