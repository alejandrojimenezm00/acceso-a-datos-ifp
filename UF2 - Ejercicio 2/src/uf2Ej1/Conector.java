package uf2Ej1;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.SQLSyntaxErrorException;
import java.sql.Statement;
import java.util.Scanner;

public class Conector {

	Connection conn1 = null;

	Scanner sc = new Scanner(System.in);

	
	public Conector(){
		
		try {
			
			String url1 = "jdbc:mysql://localhost:3306/montaña";
			String user = "root";
			String password = "";
			
			conn1 = DriverManager.getConnection(url1, user, password);
			

		} catch (SQLException ex) {
			System.out.println("ERROR:La dirección no es válida o el usuario y clave");
			ex.printStackTrace();
		}
	}
	
	public void crearTabla() {
		
		try {
			
			Statement crearTabla = conn1.createStatement();
			crearTabla.executeUpdate("CREATE TABLE Montañas" +
									 "(Nombre VARCHAR(255) not NULL, " + 
									 "Altura INT(255),  " +
									 "PrimeraAscension INT(255), " + 
									 "Región TEXT(100), " +
									 "País TEXT(1000), " +
									 "PRIMARY KEY (Nombre))");
			System.out.println("Tabla creada correctamente\n");
			
		} catch (SQLSyntaxErrorException e) {
			{
				System.out.println("Ya existe la tabla");
			}
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	// Metodo para crear montañas
	// Creo la montaña con los datos que el usuario me pasa por parametros
	
	public void crearMontania(String n, int a, int pe, String r, String p) {
		
	
		try {
					
			
			Statement crearMontania = conn1.createStatement();
			
			crearMontania.executeUpdate("INSERT INTO montañas (Nombre, Altura, PrimeraAscension, Región, País)" + 
										" VALUES (" + "'" + n + "'" + "," + a + "," + pe + "," + "'" + r + "'" + "," + "'" + p + "'" + ")");
			
			System.out.println("Datos añadidos");
			
			crearMontania.clearBatch();
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	// Metodo para consultar la tabla
	
	public void consultarTabla() {
		
		try {
			
			Statement consultarTabla = conn1.createStatement();

			ResultSet resultadoConsulta = consultarTabla.executeQuery("SELECT * FROM montañas");
			
			while (resultadoConsulta.next()) {
				
				System.out.println(resultadoConsulta.getString(1)   + "  "  + 
								   resultadoConsulta.getInt(2)      + "  "  +
								   resultadoConsulta.getInt(3)      + "  "  +
								   resultadoConsulta.getString(4)   + "  "  +
								   resultadoConsulta.getString(5) );

				
			}
			
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
	}
	
	// Metodo para devolver solo el nombre de las montañas
	
	public void devolverMonania() {
		
		 try {
			
			Statement consultarTabla = conn1.createStatement();

			ResultSet resultadoConsulta = consultarTabla.executeQuery("SELECT * FROM montañas");
			
			while (resultadoConsulta.next()) {
				
				System.out.println(resultadoConsulta.getString(1));

				
			}
			
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	// Metodo para consultar los datos de una montaña solo si se llama igual que la que nos pide el usuario
	
	public void consultarMontania(String n) {
		

		 try {
			
			Statement consultarTabla = conn1.createStatement();

			ResultSet resultadoConsulta = consultarTabla.executeQuery("SELECT * FROM montañas");
			
			while (resultadoConsulta.next()) {
				
				if (n.equals(resultadoConsulta.getString(1))) {
					
					System.out.println(resultadoConsulta.getString(1)   + "  "  + 
							   resultadoConsulta.getInt(2)      + "  "  +
							   resultadoConsulta.getInt(3)      + "  "  +
							   resultadoConsulta.getString(4)   + "  "  +
							   resultadoConsulta.getString(5) );
					
				}
				
			}
			
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	// Metodo para modificar una montaña (campos de texto)
	
	public void modificarMontania(String n, String c) {
		
		String modificacion = "";
		
		System.out.println("Valor que quieres añadir");
		modificacion = sc.nextLine();
		

		try {
			
			Statement modificarCampo = conn1.createStatement();

			modificarCampo.executeUpdate("UPDATE montañas SET " + c + "=" + "'" + modificacion + "'" + " WHERE Nombre='" + n + "'");
			
			
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
	}
	
	// Metodo para modificar una montaña (campos numéricos)
	
	public void modificarMontaniaNum(String n, String c) {
		
		int modificacion = 0;
		
		System.out.println("Valor que quieres añadir");
		modificacion = sc.nextInt();
		sc.nextLine();
		

		try {
			
			Statement modificarCampo = conn1.createStatement();

			modificarCampo.executeUpdate("UPDATE montañas SET Altura=" + modificacion + " WHERE Nombre='" + n + "'");
			
			
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
	}
	
	// Metodo para modificar una montaña (campos numéricos (ascension))
	
	public void modificarMontaniaNumAscension(String n, String c) {
		
		int modificacion = 0;
		
		System.out.println("Valor que quieres añadir");
		modificacion = sc.nextInt();
		sc.nextLine();
		

		try {
			
			Statement modificarCampo = conn1.createStatement();

			modificarCampo.executeUpdate("UPDATE montañas SET PrimeraAscension=" + modificacion + " WHERE Nombre='" + n + "'");
			
			
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
	}
	
	// Metodo para eliminar una montaña
	
	public void eliminarMonania(String n) {
		
		try {
			
			Statement eliminarCampo = conn1.createStatement();			
			
			eliminarCampo.executeUpdate("DELETE FROM montañas WHERE Nombre='" + n + "'");
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	// Metodo para eliminar la tabla
	
	public void eliminarTabla() {
		
		try {
			
			Statement eliminarCampo = conn1.createStatement();			
			
			eliminarCampo.executeUpdate("DROP TABLE montañas");
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

	public void closeConnexion(){
		
		try {

			System.out.println("Conexión terminada");
			conn1.close();

		} catch (SQLException ex) {
			System.out.println("ERROR:al cerrar la conexión");
			ex.printStackTrace();
		}
	}
}
	

