package uf1Ejercicio4;

public class Persona {
	
	private String nombre, apellido, ciudad, nacionalidad;
	private int edad;
	
	
	// Constructor para la clase persona
	
	public Persona () {
	}
	
	
	// seters
	
	// seter nombre
	public void setNombre (String n)
	{
		nombre = n;
	}
	
	// seter apellido
	public void setApellido (String a)
	{
		apellido = a;
	}
	
	// seter ciudad
	public void setCiudad (String c)
	{
		ciudad = c;
	}	
	
	// seter nacionalidad
	public void setNacionalidad(String na)
	{
		nacionalidad = na;
	}
	
	// seter edad
	public void setEdad (int e)
	{
		edad = e;
	}

	
	
	// getters

	// get nombre
	
	public String obtenerNombre() {
		return nombre;
	}
	
	// get appelido
	
	public String obtenerApellido() {
		return apellido;
	}
	
	// get ciudad
	
	public String obtenerCiudad() {
		return ciudad;
	}
	
	// get nacionalidad
	
	public String obtenerNacionalidad() {
		return nacionalidad;
	}
	
	// get edad
	
	public int obtenerEdad() {
		return edad;
	}
	
	
	// metodo para devolver toda la persona
	
	public String obtenerPersona() {
		return "nombre:" + nombre + ",apellido:" + apellido + ",edad:" + edad + ",ciudad:" + ciudad + ",nacionaliad:" + nacionalidad+"\n";
	}
	

	
	
}
