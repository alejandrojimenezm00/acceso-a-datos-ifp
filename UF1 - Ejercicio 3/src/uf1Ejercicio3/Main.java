package uf1Ejercicio3;

import java.io.*;
import java.util.Scanner;
import uf1Ejercicio3.Persona;

public class Main {

	public static void main(String[] args) {
		
		// Objeto scanner para leer datos
        Scanner sc = new Scanner(System.in);
        
        // variable de control para manejar el menu
		int eleccion = 1;
		
		// bucle while para ejecutar el menu tantas veces como quiera el usuario
		while (eleccion < 3 && eleccion > 0) {
		
			System.out.print("******************\n"
							 + "1. Escribir.\n"
							 + "2. Leer\n"
							 + "3. Salir\n"
							 + "Opcion: ");
			
			eleccion = sc.nextInt();
			sc.nextLine();
			
			switch (eleccion) {
			case 1: {
				
				// Creando un File path conseguimos comprobar la existencia del path
				
				File path = new File ("C:/ejercicio/");
				
				// Declaración del fichero a null. Dado a que si el directorio no existe no tiene sentido
				// reservar memoria al fichero.
				
				File nombresAlumnos = null;
				
				// Primero compruebo si el directorio existe. Sino lo hace no tiene sentido continuar con la
				// ejecución del código.
				
				if (!path.isDirectory()){
					
					System.out.println("El directorio no existe, Pongase en contacto con el administrador");
				}
				else {
					
					nombresAlumnos = new File (path + "/personas.txt");
					
					// Aqui creo el bucle para que el usuario cree las personas
					// Dado a que si el path es incorrecto no tiene sentido que el usuario reserve memoria o "pierda el tiempo"
					
					// Ya que el usuario va a crear 3 personas. Pienso que la mejor forma de almacenarlas es con un array
					
					Persona[] personas = new Persona[3];
					
					
					for (int i = 0; i < 3; i++) {
						
						// Llamada al constructor vacio.
						personas[i] = new Persona();
						
						System.out.println("Nombre: ");
						personas[i].setNombre(sc.nextLine());
						
						System.out.println("Apellido: ");
						personas[i].setApellido(sc.nextLine());
						
						// Aqui, antes de pasar al siguiente campo leo una linea vacia para limpiar el buffer
						System.out.println("Edad: ");
						personas[i].setEdad(sc.nextInt());
						sc.nextLine();
						
						System.out.println("Ciudad: ");
						personas[i].setCiudad(sc.nextLine());
						
						System.out.println("Nacionalidad: ");
						personas[i].setNacionalidad(sc.nextLine());
					}
					
					
					
					
					if (nombresAlumnos.exists())
					{
						System.out.println("El fichero existe, se van a introducir datos");
						
						try {
							
							// Linea para abrir el fichero en modo escritura. con el parametro true hago un append.
							// lo que me mantiene los datos que hubiese antes
							
							
							BufferedWriter bw = new BufferedWriter(new FileWriter(nombresAlumnos, true));
							
							for (int i = 0; i < 3; i++)
							{
								bw.write(personas[i].obtenerPersona());
								bw.write("\n");
							}
							
							// Con el metodo close() cierro el fichero y se guardan los cambios
							
							bw.close();
							
						} catch (IOException e) {
							
							System.out.println("Error al leer el fichero");

						}
						
					}
					else {
						
						System.out.println("El fichero no existe, se crea exitosamente");
						
						try {
							
							// Reutilizo la linea porque en caso de que no lo encuentre, BufferedWritter crea el fichero
							
							BufferedWriter bw = new BufferedWriter(new FileWriter(nombresAlumnos));
							
							for (int i = 0; i < 3; i++)
							{
								bw.write(personas[i].obtenerPersona());
								bw.write("\n");
							}
							
							// Con el metodo close() cierro el fichero y se guardan los cambios
							
							bw.close();
							
						} catch (IOException e) {
							
							System.out.println("Error al crear el fichero");
							
						}
					}

				}
				
				break;

			}
			case 2: {
				
				String ubicacion = "";
				String nombre = "";
				
				// Con las siguientes lineas declaro el nombre del fichero y opero solo si existo
				
				System.out.print("Ubicación del archivo: ");
				ubicacion = sc.nextLine();
				System.out.println("Nombre del archivo: ");
				nombre = sc.nextLine();
				
				File ubicacionArchivo = new File (ubicacion);
				File rutaTotal = new File (ubicacion + "/" + nombre);
				
				if (ubicacionArchivo.isDirectory() && rutaTotal.exists()){
					
					// Variable para controlar la lectura del menu
					
					int eleccionLectura = 1;

					while (eleccionLectura < 3 && eleccionLectura > 0) {
						
						System.out.print("\n\nElije la opcion.\n"
						         + "1. Leer una persona\n"
						         + "2. Leer fichero\n"
						         + "3. Salir.\n"
						         + "Opcion: ");
						eleccionLectura = sc.nextInt();
						sc.nextLine();
						
						switch (eleccionLectura){
						
						case 1: {
							
							try {
								
								// Con esto abro el fichero con el buffered reader para operar con las lineas enteras
								
								BufferedReader br = new BufferedReader(new FileReader(rutaTotal.toString()));
								String lineaActual = "";
								String nombrePersonaBuscada = "";
								
								// Mi idea, por como he planteado el fichero con los nombres es primero spliteear la linea separando los campos
								// del nombre, apellidos, etc... Y luego volver a Splitear para quedarme solo con el nombre
								
								System.out.println("Nombre de la persona que quieres buscar: ");
								nombrePersonaBuscada = sc.nextLine();
								
								while ((lineaActual = br.readLine()) != null){									
									
									String lineaActualFormateada[] = lineaActual.split(",");
									String nombrePersona[] = lineaActualFormateada[0].split(":");									
									
									// Con esto imprimo las lineas que solo coinciden con lo que busca el usuario
									
									if (nombrePersona[1].equals(nombrePersonaBuscada)){
										System.out.println(lineaActual);
									}
									
									
								}
								
								br.close();
							
							} catch (FileNotFoundException e) {
								System.out.println("Existe un error al leer el fichero. Comprueba la ruta");
								
							} catch (IOException e) {
								System.out.println("Existe un error al leer el fichero. Comprueba la ruta");
							}
							
							break;

							
						}
						case 2: {
							
							try {
								
								// Con esto abro el fichero de la misma forma que en el caso 1 pero ahora imprimo todo
								
								BufferedReader br = new BufferedReader(new FileReader(rutaTotal.toString()));
								String lineaActual = "";
								
								while ((lineaActual = br.readLine()) != null){
									System.out.println(lineaActual);
								}
								
								br.close();
								
							} catch (FileNotFoundException e) {
								System.out.println("Existe un error al leer el fichero. Comprueba la ruta");
								
							} catch (IOException e) {
								System.out.println("Existe un error al leer el fichero. Comprueba la ruta");
							}
							
							break;
							

							
						}
						
						}
						
					}
					
				
				} 
				else {
					System.out.println("El fichero o la ruta introducidos no existen");
				}
			
				break;

			}
			


			
		};
			
		}
	}
}
		
