package uf1Ejercicio5;

import java.io.*;
import java.util.Scanner;
import uf1Ejercicio5.Persona;

public class Main {

	// Objeto scanner para leer datos. Lo declaro antes de los metodos para que se use de forma global.
    static Scanner sc = new Scanner(System.in);
	
	private static void escribirFicheroTexto () {
		
		// Creando un File path conseguimos comprobar la existencia del path
		
		File path = new File ("C:/ejercicio/");
		
		// Declaración del fichero a null. Dado a que si el directorio no existe no tiene sentido
		// reservar memoria al fichero.
		
		File nombresAlumnos = null;
		
		// Primero compruebo si el directorio existe. Sino lo hace no tiene sentido continuar con la
		// ejecución del código.
		
		if (!path.isDirectory()){
			
			System.out.println("El directorio no existe, Pongase en contacto con el administrador");
		}
		else {
			
			nombresAlumnos = new File (path + "/personas.txt");
			
			// Aqui creo el bucle para que el usuario cree las personas
			// Dado a que si el path es incorrecto no tiene sentido que el usuario reserve memoria o "pierda el tiempo"
			
			// Ya que el usuario va a crear 3 personas. Pienso que la mejor forma de almacenarlas es con un array
			
			Persona[] personas = new Persona[3];
			
			
			for (int i = 0; i < 3; i++) {
				
				// Llamada al constructor vacio.
				personas[i] = new Persona();
				
				System.out.println("Nombre: ");
				personas[i].setNombre(sc.nextLine());
				
				System.out.println("Apellido: ");
				personas[i].setApellido(sc.nextLine());
				
				// Aqui, antes de pasar al siguiente campo leo una linea vacia para limpiar el buffer
				System.out.println("Edad: ");
				personas[i].setEdad(sc.nextInt());
				sc.nextLine();
				
				System.out.println("Ciudad: ");
				personas[i].setCiudad(sc.nextLine());
				
				System.out.println("Nacionalidad: ");
				personas[i].setNacionalidad(sc.nextLine());
			}
			
			
			
			
			if (nombresAlumnos.exists())
			{
				System.out.println("El fichero existe, se van a introducir datos");
				
				try {
					
					// Linea para abrir el fichero en modo escritura. con el parametro true hago un append.
					// lo que me mantiene los datos que hubiese antes
					
					
					BufferedWriter bw = new BufferedWriter(new FileWriter(nombresAlumnos, true));
					
					for (int i = 0; i < 3; i++)
					{
						bw.write(personas[i].obtenerPersona());
						bw.write("\n");
					}
					
					// Con el metodo close() cierro el fichero y se guardan los cambios
					
					bw.close();
					
				} catch (IOException e) {
					
					System.out.println("Error al leer el fichero");

				}
				
			}
			else {
				
				System.out.println("El fichero no existe, se crea exitosamente");
				
				try {
					
					// Reutilizo la linea porque en caso de que no lo encuentre, BufferedWritter crea el fichero
					
					BufferedWriter bw = new BufferedWriter(new FileWriter(nombresAlumnos));
					
					for (int i = 0; i < 3; i++)
					{
						bw.write(personas[i].obtenerPersona());
						bw.write("\n");
					}
					
					// Con el metodo close() cierro el fichero y se guardan los cambios
					
					bw.close();
					
				} catch (IOException e) {
					
					System.out.println("Error al crear el fichero");
					
				}
			}

		}
		
	}
	private static void leerFicheroTexto () {
		
		// Varaiables para almacenar información sobre el fichero
		
		String ubicacion = "";
		String nombre = "";
		
		// Almacenamiento del fichero

		System.out.print("Ubicación del archivo: ");
		ubicacion = sc.nextLine();
		System.out.println("Nombre del archivo: ");
		nombre = sc.nextLine();
		
		File ubicacionArchivo = new File (ubicacion);
		File rutaTotal = new File (ubicacion + "/" + nombre);
		
		if (ubicacionArchivo.isDirectory() && rutaTotal.exists()){
			
			int eleccionLectura = 1;

			while (eleccionLectura < 3 && eleccionLectura > 0) {
				
				System.out.print("\n\nElije la opcion.\n"
				         + "1. Leer una persona\n"
				         + "2. Leer fichero\n"
				         + "3. Salir.\n"
				         + "Opcion: ");
				eleccionLectura = sc.nextInt();
				sc.nextLine();
				
				switch (eleccionLectura){
				
				case 1: {
					
					try {
						
						// Con estas lineas abro el fichero con un bufferedReader
						
						BufferedReader br = new BufferedReader(new FileReader(rutaTotal.toString()));
						String lineaActual = "";
						String nombrePersonaBuscada = "";
						
						System.out.println("Nombre de la persona que quieres buscar: ");
						nombrePersonaBuscada = sc.nextLine();
						
						// Mi idea, por como he planteado el fichero con los nombres es primero spliteear la linea separando los campos
						// del nombre, apellidos, etc... Y luego volver a Splitear para quedarme solo con el nombre
						while ((lineaActual = br.readLine()) != null){									
							
							// Con esto imprimo las lineas que solo coinciden con lo que busca el usuario
							String lineaActualFormateada[] = lineaActual.split(",");
							String nombrePersona[] = lineaActualFormateada[0].split(":");									
							
							if (nombrePersona[1].equals(nombrePersonaBuscada)){
								System.out.println(lineaActual);
							}
							
							
						}
						
						br.close();
					
					} catch (FileNotFoundException e) {
						System.out.println("Existe un error al leer el fichero. Comprueba la ruta");
						
					} catch (IOException e) {
						System.out.println("Existe un error al leer el fichero. Comprueba la ruta");
					}
					
					break;

					
				}
				case 2: {
					
					try {
						
						// Con esto abro el fichero de la misma forma que en el caso 1 pero ahora imprimo todo
						
						BufferedReader br = new BufferedReader(new FileReader(rutaTotal.toString()));
						String lineaActual = "";
						
						while ((lineaActual = br.readLine()) != null){
							System.out.println(lineaActual);
						}
						
						br.close();
						
					} catch (FileNotFoundException e) {
						System.out.println("Existe un error al leer el fichero. Comprueba la ruta");
						
					} catch (IOException e) {
						System.out.println("Existe un error al leer el fichero. Comprueba la ruta");
					}
					
					break;
					

					
				}
				
				}
				
			}
			
		
		} 
		else {
			System.out.println("El fichero o la ruta introducidos no existen");
		}
	}
	private static void escribirFicheroBinario () {
		
		try {
			
			// Definicion del fichero
			FileOutputStream ficheroBinario = new FileOutputStream("C:/ejercicio/datosPersona.dat", true);
			
			BufferedReader br = new BufferedReader(new FileReader("C:/ejercicio/datosPersona.dat"));     
			if (br.readLine() == null) {
			    System.out.println("El fichero esta vacio");
			}			
			// Definicion de la escritura del fichero
			DataOutputStream escrituraFicheroBinario = new DataOutputStream(ficheroBinario);
			
			int numPersonas = 0;
			System.out.println("Número de personas que se quieren añadir (máximo 3): ");
			numPersonas = sc.nextInt();
			sc.nextLine();
			
			if (numPersonas >= 4 || numPersonas < 0) {
				
				System.out.println("No se pueden añadir mas de 3 personas");
				
			} 
			else {
				
				Persona personas[] = new Persona[numPersonas]; 
				int contador = 0;
				
				while (contador != numPersonas) {
					
					// Llamada al constructor vacio.
					personas[contador] = new Persona();
					
					System.out.println("Nombre: ");
					personas[contador].setNombre(sc.nextLine());
					
					System.out.println("Apellido: ");
					personas[contador].setApellido(sc.nextLine());
					
					// Aqui, antes de pasar al siguiente campo leo una linea vacia para limpiar el buffer
					System.out.println("Edad: ");
					personas[contador].setEdad(sc.nextInt());
					sc.nextLine();
					
					System.out.println("Ciudad: ");
					personas[contador].setCiudad(sc.nextLine());
					
					System.out.println("Nacionalidad: ");
					personas[contador].setNacionalidad(sc.nextLine());
					
					contador++;
					
				}
				
				for (int i = 0; i < numPersonas; i++) {
					
					escrituraFicheroBinario.writeBytes(personas[i].obtenerPersona() + "\n");
					
				}
				
			}
			
			
			
		} catch (FileNotFoundException e) {
			System.out.println("Error al leer el fichero");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public static void leerFicheroBinario() {
		

		// Variables de control
		
		String nombreBuscar = "";
		String linea = "";
		
		System.out.println("Nombre que quieres buscar: ");
		nombreBuscar = sc.nextLine();
		
		// Con este try abro el fichero binario 
		
		try (DataInputStream lecturaDatos = new DataInputStream(new FileInputStream("C:\\ejercicio\\datosPersona.dat"));) {
			
			while (true) {
				
				// Con esta linea cargo la linea en la que se encuentra el bucle. El metodo esta deprecado, sin embargo se puede serguir usando.
				
				linea = lecturaDatos.readLine();
				
				// Con este ignoro las lineas vacias
				
				if (linea != null) {
					
					// Estas líneas realizan lo mismo que con los ficheros de texto
					
					String[] lineaCompuesta = linea.split(",");
					String[] nombre = lineaCompuesta[0].split(":");
					
					if (nombre[1].equals(nombreBuscar)){
						
						System.out.println(linea);
						
					}
					
				} else {
					throw new EOFException ("Fin del fichero");
				}							
				
			}
			
		} catch (EOFException e) {
			System.out.println("Fin del fichero");
		} catch (IOException e) {
			System.out.println(e);
		}
		
		
	}
	
	public static void main(String[] args) {
		
		
        
        // variable de control para manejar el menu
		int eleccion = 1;
		
		// bucle while para ejecutar el menu tantas veces como quiera el usuario
		while (eleccion < 5 && eleccion > 0) {
		
			System.out.print("******************\n"
							 + "1. Escribir.\n"
							 + "2. Leer\n"
							 + "3. Escribir (fichero binario) \n"
							 + "4. Leer (fichero binario) \n"
							 + "5. Salir\n"
							 + "Opcion: ");
			
			eleccion = sc.nextInt();
			sc.nextLine();
			
			switch (eleccion) {
			case 1: {
				
				//Llamada al metodo para escribir en el fichero de texto
				escribirFicheroTexto();
				break;

			}
			case 2: {

				//Llamada al metodo para leer en el fichero de texto
				leerFicheroTexto();
				break;

			}
			case 3: {
				
				// llamada al metodo para escribir en el fichero binario
				escribirFicheroBinario ();
				break;
				
			}
			case 4: {
				
				leerFicheroBinario();
				break;
				
			}
			


			
		};
			
		}
	}
}
