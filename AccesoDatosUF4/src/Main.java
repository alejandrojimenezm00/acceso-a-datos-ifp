import analisis.GeneraAnalisis;
import analisis.Paciente;

public class Main {

	public static void main(String[] args) {
		//Crea un paciente nombre, apellido, edad, telefono, hierro, urea.
		Paciente paciente1 = new Paciente ("Alejandro", "Jiménez", 21, 655941347, 20, 5000); 
		
		//Creación del analisis
		GeneraAnalisis analisis1 = new GeneraAnalisis();
		
		//Adición del paciente
		analisis1.setPaciente(paciente1);
		
		//Listener para detectar cambios
		paciente1.addPropertyChangeListener(analisis1);
		
		//Adicion de datos para lanzar el listener
		paciente1.setUltimoHierro(12);
	}

}
