package analisis;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.Serializable;
import java.util.Date;
import java.beans.*;

//Escucha evento
public class GeneraAnalisis implements Serializable, PropertyChangeListener{
	
	//Generación de variables
	private int numAnalisis;
	private Paciente paciente;
	private Date fecha;
	private int analisisPendiente;
	
	// Constructores
	public GeneraAnalisis() {
		
	}
	
	public GeneraAnalisis (int nA, Paciente p, Date f, int aP) {
		numAnalisis = nA;
		paciente = p;
		fecha = f;
		analisisPendiente = aP;
	}
	
	//Autogeneramos getters and setters con eclipse
	public int getNumAnalisis() {
		return numAnalisis;
	}

	public void setNumAnalisis(int numAnalisis) {
		this.numAnalisis = numAnalisis;
	}

	public Paciente getPaciente() {
		return paciente;
	}

	public void setPaciente(Paciente paciente) {
		this.paciente = paciente;
	}

	public Date getFecha() {
		return fecha;
	}

	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}

	public int getAnalisisPendiente() {
		return analisisPendiente;
	}

	public void setAnalisisPendiente(int analisisPendiente) {
		this.analisisPendiente = analisisPendiente;
	}

	public void propertyChange(PropertyChangeEvent evt) {
		System.out.printf("Hierro anterior: %d%n", evt.getOldValue());
		System.out.printf("Hierro actual: %d%n", evt.getNewValue());
		
		System.out.printf("Realizar analisis proximamente: %s %s%n", paciente.getNombre(), paciente.getApellido());
	}
}
