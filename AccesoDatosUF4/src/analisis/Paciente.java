package analisis;
import java.io.Serializable;
import  java.beans.*;
//Genera eventos 
public class Paciente implements Serializable {
	//Creación de variables
	private String nombre;
	private String apellido;
	private int edad;
	private int telefono;
	private int ultimoHierro;
	private int ultimoUrea;
	
	//Genera eventos cuando la propiedad cambia y notifica a los dem�s.
	private PropertyChangeSupport propertySupport;
	
	//Constructor del paciente
	public Paciente(String n, String a, int e, int t, int uH, int uU) {
		propertySupport = new PropertyChangeSupport (this);
		nombre = n;
		apellido = a;
		edad = e;
		telefono = t;
		ultimoHierro = uH;
		ultimoUrea = uU;
	}
	
	
	//Getters y Setters 
	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApellido() {
		return apellido;
	}

	public void setApellido(String apellido) {
		this.apellido = apellido;
	}

	public int getEdad() {
		return edad;
	}

	public void setEdad(int edad) {
		this.edad = edad;
	}

	public int getTelefono() {
		return telefono;
	}

	public void setTelefono(int telefono) {
		this.telefono = telefono;
	}

	public int getUltimoHierro() {
		return ultimoHierro;
	}

	public void setUltimoHierro(int valorNuevo) {
		//creamos variable para guardar el valor anterior
		int valorAnterior = ultimoHierro;
		//creamos varialbe para guardar el valor nuevo y poder compararlo en el evento
		ultimoHierro = valorNuevo;
		//evento compara el valor anterior con el valor nuevo del Hierro y si es mayor que 10 salta.
		if (valorNuevo > 10) {
			propertySupport.firePropertyChange("ultimoHierro", valorAnterior, valorNuevo);
		}
	}

	public int getUltimoUrea() {
		return ultimoUrea;
	}

	public void setUltimoUrea(int ultimoUrea) {
		this.ultimoUrea = ultimoUrea;
	}

	public PropertyChangeSupport getPropertySupport() {
		return propertySupport;
	}

	public void setPropertySupport(PropertyChangeSupport propertySupport) {
		this.propertySupport = propertySupport;
	}

	public void addPropertyChangeListener (PropertyChangeListener listener) {
		propertySupport.addPropertyChangeListener(listener);
	}
	
	public void removePropertyChangeListener (PropertyChangeListener listener) {
		propertySupport.removePropertyChangeListener(listener);
	}
}
