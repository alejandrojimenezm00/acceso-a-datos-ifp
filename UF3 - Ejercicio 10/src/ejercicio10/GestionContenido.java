package ejercicio10;

import java.util.ArrayList;

import org.xml.sax.Attributes;
import org.xml.sax.helpers.DefaultHandler;

public class GestionContenido extends DefaultHandler{

	// Declaracion del array list
	static ArrayList<String> alumnos;
	
	// creo el valor del array list al ser llamado
	public GestionContenido() {
		super();
		alumnos = new ArrayList<String>();
		
	}
	
	public void startDocument() {
		System.out.println("Comienzo el documento XML");
	}
	
	public void enDocument() {
		System.out.println("Final del documento");
	}
	
	
	// En lugar de imprimir por pantalla los nombres de los elemenos, los añado al array list
	public void startElement (String uri, String nombre, String nombreC, Attributes atts) {
		alumnos.add(nombre);
	}
	
	public void endElement (String uri, String nombre, String nombreC, Attributes atts) {
		alumnos.add(nombre);
	}
	
	public void characters (char[] ch, int inicio, int longitud) {
		
		String car = new String (ch, inicio, longitud);
		car = car.replaceAll("[\t\n]", "");
		alumnos.add(car);
		
	}
	
	public static void recorrerArrayList () {
		
		for (int i = 0; i < alumnos.size(); i++) {
			
			System.out.println(alumnos.get(i));
			
		}
		
	}
	
}